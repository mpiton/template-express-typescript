# Template Express

![Logo](https://cloud.4o4.fr/nextcloud/index.php/s/2QiqFQs8gx4XF9t/preview)

<br>
<br>

<div style="font-style: italic; text-align: center;" markdown="1">

### My preconfigured Express Template for TypeScript & MongoDB

[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://github.com/tterb/atomic-design-ui/blob/master/LICENSEs)

</div>
<br>
<br>

## Table of Contents

- [Prerequisite](#prerequisite)
- [Overview](#overview)
- [Getting Started](#getting-started)
  - [Installation](#installation)
  - [Directory Structure](#directory-structure)
  - [Available Routes](#available-routes)
  - [Config](#config)
- [Alias](#alias)
- [Local Development](#local-development)
- [Production](#production)
- [Contribution](#contribution)
- [Author](#author)
  <br>
  <br>

## Prerequisite

- [Node.js](https://nodejs.org) `(>= 12.0.0)`
- [Yarn](https://yarnpkg.com/en/docs/install) or [NPM](https://docs.npmjs.com/getting-started/installing-node)
- You need to install [MongoDB](https://docs.mongodb.com/manual/administration/install-community/) either on your local machine or using a cloud service as [Atlas](https://www.mongodb.com/fr-fr/cloud/atlas)

## Overview

This template is made to use Typescript with Express and MongoDB

- Everything typed with [TypeScript](https://www.typescriptlang.org/)
- Framework: [Express.js](https://expressjs.com/)
- ODM: [Mongoose](https://mongoosejs.com/)
- Authentication & Authorization with [JSON Web Token](https://jwt.io/)
- [ES6](http://babeljs.io/learn-es2015/) features/modules
- ES7 [async](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function) / [await](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await)
- Run with [Nodemon](https://nodemon.io/) for automatic reload & watch
- [ESLint](http://eslint.org/) for code linting
- [Morgan](https://github.com/expressjs/morgan) for logging request

- Configuration management using [dotenv](https://www.npmjs.com/package/dotenv)
- Manage production app proccess with [PM2](https://pm2.keymetrics.io/)

## Getting Started

### Installation

Install the dependencies using with npm

```bash
  cd my-project
  npm install
```

### Directory Structure

```

src
├─ app.ts
├─ config
│  └─ index.ts
├─ controller
│  ├─ auth.ts
│  └─ home.ts
├─ db
│  └─ db.ts
├─ index.ts
├─ middlewares
│  └─ errorHandler.ts
├─ models
│  └─ User.ts
├─ server.ts
├─ service
│  ├─ auth.ts
│  └─ home.ts
├─ types
│  ├─ request
│  └─ response
│     └─ AppInformation.ts
└─ utils
    ├─ array.ts
    ├─ crypt.ts
    ├─ error.ts
    ├─ filter.ts
    ├─ object.ts
    └─ url.ts

```

### Available Routes

##### Login User

```http
  GET /account
```

| Request Body | Type     | Description                 |
| :----------- | :------- | :-------------------------- |
| `username`   | `string` | **Required**. Your username |
| `password`   | `string` | **Required**. Your password |

##### Create User

```http
  POST /account/create
```

| Request Body | Type     | Description                 |
| :----------- | :------- | :-------------------------- |
| `username`   | `string` | **Required**. Your username |
| `password`   | `string` | **Required**. Your password |

### Config

- Rename your file `.env.example` to `.env`

```bash
  cp .env.example .env
```

- Add or modify specific variables and update it according to your need.
- Check the `config` folder to customize your settings (`/src/config`)

## Alias @

To make paths clean and ease to access `@` is setup up for `/src` path

```javascript
// BEFORE
import config from "./config";
import routes from "./routes";

// NOW
import config from "@/config";
import routes from "@/routes";
```

> You can customize this setup:
> `/tsconfig.json` > compilerOptions.paths

## Local Development

Run the server locally. It will be run with Nodemon and ready to serve on port `8080` (unless you specify it on your `.env`)

```bash
  npm start
```

> Check [`package.json`](https://gitlab.com/mpiton/template-express-typescript/-/blob/main/package.json) to see more "scripts"

## Production

First, build the application.

```bash
 yarn build # or npm run build
```

Then, use [`pm2`](https://github.com/Unitech/pm2) to start the application as a service.

```bash
 yarn service:start # or npm run service:start
```

## Contribution

Contributions are always welcome!
See `contributing.md` for ways to get started.
Please adhere to this project's `code of conduct`.

## Author

<a href="https://gitlab.com/mpiton" target="_blank" rel="noopener" >
<img width="60px" style="border-radius: 50%;" src=https://gitlab.com/uploads/-/system/user/avatar/6437144/avatar.png?width=400">
</a>
