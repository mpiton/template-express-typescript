import mongoose from "mongoose";
import { env } from "process";

// mongodb connection
export const connect = (): void => {
	if (env.DB_HOST && env.DB_NAME) {
		const mongoDb = env.DB_HOST + env.DB_NAME;
		mongoose.connect(mongoDb, () => console.log("👍 MongoDB Connected 👍"));
		const db = mongoose.connection;
		db.on("error", console.error.bind(console, "MongoDB Connection error"));
	}
};
