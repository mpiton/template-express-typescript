import * as homeController from "@/controller/home";

import { Router } from "express";
import * as authController from "@/controller/auth";

const router = Router();

router.get("/", homeController.getAppInfo);

//AUTH
router.get("/account", authController.login);
router.post("/account/create", authController.signup);

export default router;
