import * as authService from "@/service/auth";
import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import CONFIG from "@/config";
import { UserDocument } from "@/models/User";

/**
 * Get the User information.
 *
 * @param {Request} req
 * @param {Response} res
 * @param  {NextFunction} next
 * @returns Promise<Response<{ success: boolean; data: UserDocument; token: string }> | undefined>
 */
export const login = async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<
	Response<{ success: boolean; data: UserDocument; token: string }> | undefined
> => {
	try {
		const username: string = req.body.username;
		const password: string = req.body.password;
		const user: UserDocument | null = await authService.findUserByUsername(
			username
		);
		if (user) {
			const isPasswordValid = await user.comparePassword(password);
			const jwtSecret = CONFIG.AUTH.JWT_SECRET;
			const tokenExpirationInSeconds = CONFIG.AUTH.JWT_TOKEN_EXPIRE;
			if (!isPasswordValid) {
				throw new Error("Invalid Password");
			} else {
				const token = jwt.sign(req.body, jwtSecret, {
					expiresIn: tokenExpirationInSeconds,
				});
				return res.status(200).json({
					success: true,
					data: user,
					token,
				});
			}
		} else {
			throw new Error("User Not Found");
		}
	} catch (error) {
		next(error);
	}
};

/**
 * Get the user created
 *
 * @param req
 * @param res
 * @param  {NextFunction} next
 * @returns Promise<Response<{ success: boolean; data: UserDocument; jwt: string }> | undefined>
 */
export const signup = async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<
	Response<{ success: boolean; data: UserDocument; jwt: string }> | undefined
> => {
	try {
		const username = req.body.username;
		const password = req.body.password;
		const user = await authService.findUserByUsername(username);
		if (user) {
			throw new Error("User already exists");
		} else {
			try {
				const jwtSecret = CONFIG.AUTH.JWT_SECRET;
				const tokenExpirationInSeconds = CONFIG.AUTH.JWT_TOKEN_EXPIRE;
				const newUser = await authService.createUser(req);

				const token = jwt.sign({ username, password }, jwtSecret, {
					expiresIn: tokenExpirationInSeconds,
				});
				return res
					.status(200)
					.json({ success: true, data: newUser, jwt: token });
			} catch (error) {
				throw new Error("Error while register");
			}
		}
	} catch (error) {
		next(error);
	}
};
