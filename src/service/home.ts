import AppInformation from "@/types/response/AppInformation";
import CONFIG from "@/config";
import User from "@/models/User";
import { Response, Request } from "express";

/**
 * Get All Users
 * @param _req
 * @param res
 *
 */
export const getUser = (_req: Request, res: Response): void => {
	try {
		User.find();
	} catch (error) {
		res.status(500).send(error);
	}
};

/**
 * Get application information.
 *
 * @returns {AppInformation}
 */
export const getAppInfo = (): AppInformation => {
	return CONFIG.APP;
};
