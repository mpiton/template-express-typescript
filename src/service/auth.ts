import User, { UserDocument } from "@/models/User";
import { Request } from "express";

/**
 * Creation of the user
 *
 * @param req
 * @returns Promise<UserDocument>
 */
export const createUser = async (req: Request): Promise<UserDocument> => {
	try {
		const user: UserDocument = new User({
			username: req.body.username,
			password: req.body.password,
			lastAuthentication: new Date(),
		});
		await user.save();
		return user;
	} catch (error) {
		throw new Error(error);
	}
};

/**
 * Find the User by username
 *
 * @param username
 * @returns Promise<UserDocument | null>
 */
export const findUserByUsername = async (
	username: string
): Promise<UserDocument | null> => {
	try {
		const user: UserDocument | null = await User.findOne({
			username: username,
		});
		return user;
	} catch (error) {
		throw new Error(error);
	}
};
