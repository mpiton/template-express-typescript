import mongoose from "mongoose";
import bcrypt from "bcrypt";
import config from "config";

export interface UserDocument extends mongoose.Document {
	username: string;
	password: string;
	lastAuthentication: Date;
	createdAt: Date;
	updatedAt: Date;
	comparePassword(candidatePassword: string): Promise<boolean>;
}

const UserSchema = new mongoose.Schema(
	{
		_id: { type: String },
		username: { type: String, required: true, unique: true },
		password: { type: String, required: true },
		lastAuthentication: { type: Date, required: true },
	},
	{ timestamps: true }
);

UserSchema.pre("save", async function (next) {
	const user = this as UserDocument;

	// only hash the password if it has been modified (or is new)
	if (!user.isModified("password")) return next();

	// Random additional data
	const salt = await bcrypt.genSalt(+config.AUTH.SALT_ROUNDS);

	const hash = bcrypt.hashSync(user.password, salt);

	// Replace the password with the hash
	user.password = hash;

	return next();
});

// Used for logging in
UserSchema.methods.comparePassword = async function (
	candidatePassword: string
): Promise<boolean> {
	const user = this as UserDocument;

	return bcrypt.compare(candidatePassword, user.password).catch(() => false);
};

const User = mongoose.model<UserDocument>("User", UserSchema);

export default User;
